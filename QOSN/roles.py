from rolepermissions.roles import AbstractUserRole

class Admin(AbstractUserRole):
    available_permissions = {
        'created_game' : True,
        'update_game' : True,
        'delete_game' : True,
    }

class Normal(AbstractUserRole):
    available_permissions = {
        'read_games' : True,
        'created_game' : False,
        'update_game' : False,
        'delete_game' : False,
    }
