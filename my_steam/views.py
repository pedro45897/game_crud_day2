# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from .models import Game, UserGames, Profile
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import AddGameForm, UserRegistrationForm, ProfileForm
from django.core.exceptions import ValidationError
import services

from rolepermissions.mixins import HasRoleMixin
from rolepermissions.roles import assign_role

class GameListView(LoginRequiredMixin,generic.ListView):
    model = Game
    context_object_name = 'game_list'

    def get_context_data(self, **kwargs):
        context = super(GameListView, self).get_context_data(**kwargs)
        context['extra_data'] = 'This data is just an example'
        return context

class UserGamesListView(LoginRequiredMixin, generic.ListView):
    model = Game
    context_object_name = 'game_list'

    def get_queryset(self):
        return Game.objects.filter(owners=self.request.user)

class GameDetailView(LoginRequiredMixin, generic.DetailView):
    model = Game
    paginate_by = 5

class GameCreate(HasRoleMixin, LoginRequiredMixin, CreateView):
    allowed_roles = 'admin'
    model = Game
    form_class = AddGameForm

class GameUpdate(HasRoleMixin, LoginRequiredMixin, UpdateView):
    allowed_roles = 'admin'
    model = Game
    fields = ['title','publisher','platform','cover_img','genre']

class GameDelete(HasRoleMixin, LoginRequiredMixin, DeleteView):
    allowed_roles = 'admin'
    model = Game
    success_url = reverse_lazy('games')

@login_required
def index(request):
    num_games = Game.objects.all().count()
    return render(request,'index.html',context={'num_games':num_games})

# @has_role_decorator('doctor')
def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            userObj = form.cleaned_data
            username = userObj['username']
            email =  userObj['email']
            password =  userObj['password']
            if not (User.objects.filter(username=username).exists() or User.objects.filter(email=email).exists()):
                user = User.objects.create_user(username, email, password)
                profile_form = ProfileForm(request.POST, instance=user.profile)
                assign_role(user, 'normal')
                if profile_form.is_valid():
                    profile_form.save()
                user = authenticate(username = username, password = password)
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                raise ValidationError('Looks like a username with that email or password already exists')
    else:
        form = UserRegistrationForm()
        profile_form = ProfileForm()
    return render(request, 'user_form.html', {'form' : form, 'profile_form': profile_form})

@login_required
def add_game_to_collection(request, pk):
    if request.method == 'GET':
        requested_game = get_object_or_404(Game, pk=pk)
        UserGames.objects.create(user=request.user, game=requested_game)
        return redirect(index)

@login_required
def get_steam_games(request):
    if request.method == 'GET':
        app_list = services.get_player_apps(request.user.profile.steam_id)
        for app in app_list:
            app_data = services.get_app_data(app)
            if app_data['success'] == True:
                d = app_data['data']
                game = Game.objects.create(title=d['name'],publisher=d['publishers'],
                    genre=d['genres'][0]['description'],cover_img=d['header_image'])
                UserGames.objects.create(user=request.user, game=game)
        return redirect(index)
